# 如何编写插件
## 快速创建插件项目
```sh
> npm run newplugin ./path/to/plugin # 插件目录
# 此操作会将生成的插件安装成本地依赖包，请在 bdsx 目录下执行
```

## 构建插件
* 使用 VSCode
Ctrl+Shift+B -> tsc: watch

* 使用命令行
```sh
> npm run watch
```

## 安装本地插件
将插件复制到 `plugins/` 目录

## 将插件发布至 NPM
1. 注册一个 NPM 账户（如无）[NPM 注册](https://www.npmjs.com/signup)
2. 在 [Discord 服务器](https://discord.gg/pC9XdkC) 的 `#npm-bdsx-org-member-request` 频道下请求加入 `@bdsx` 组织，附上你的 NPM 账号或邮箱。
3. 使用命令行登录
```sh
> npm login
npm notice Log in on https://registry.npmjs.org/
Username: # 输入用户名
Password: # 输入密码
Email: (this IS public) # 输入邮箱
```
4. 使用命令行发布
```sh
> cd path/to/plugin # 跳转至插件目录
> npm publish --access=public # 发布插件
```

## 从 NPM 安装插件
* 使用命令行
```sh
> npm i @bdsx/插件名
```
* 使用插件管理器
运行 `plugin-manager.bat/sh`.  
搜索插件并选择要安装的插件 
选择插件的版本


## 移除从 NPM 安装的插件
* 使用命令行
```sh
> npm r @bdsx/插件名
```
* 使用插件管理器
运行 `plugin-manager.bat/sh`.  
搜索插件并选择要安装的插件 
选择 `移除`

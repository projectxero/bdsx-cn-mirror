# BDSX : BDS + node.js
![logo](bdsx/images/icon.png)  
BDSX 是一个支持 [node.js](https://nodejs.org/) 的 Minecraft 基岩版专用服务器（BDS）修改版本。基于官方 BDS 实现，包含了原版 Minecraft 的所有特性，并支持 Hook 函数与网络数据包以添加或修改服务器的功能与表现。

> 本仓库为 [原仓库](https://github.com/bdsx/bdsx) `stable` 分支的中文本地化镜像。对文档与部分提示进行了翻译，并将 BDSX Core 模块的下载来源更改为大陆区域的 CDN。

## 特性

* 操作系统：Windows（推荐）、安装了 Wine 的 Linux（不稳定）
* 所有 BDS 支持的功能与特性
* 所有 node.js 支持的功能与特性（注：基于 ChakraCore 实现。参见 [可用的 NPM 包](https://github.com/bdsx/bdsx/wiki/Available-NPM-Modules) 以了解更多）
* 使用 Visual Studio Code 进行调试（插件也可调试）
* 截取网络数据包
* [自定义命令](https://github.com/bdsx/bdsx/wiki/Custom-Commands)
* 底层 Hook 与 [DLL 调用](https://github.com/bdsx/bdsx/wiki/Call-DLL-Directly)
* 获取 IP 地址与 Xbox 用户 ID（见下方例子）

```ts
import { events } from "bdsx/event";
import { MinecraftPacketIds } from 'bdsx/bds/packetids';
events.packetAfter(MinecraftPacketIds.Login).on((ptr, networkIdentifier, packetId)=>{
    const ip = networkIdentifier.getAddress();
    if (ptr.connreq === null) return; // 客户端版本不匹配
    const cert = ptr.connreq.cert;
    const xuid = cert.getXuid();
    const username = cert.getId();
    console.log(`连接: ${username}> IP=${ip}, XUID=${xuid}`);
});
```

## 用法
* 基本配置
    * 安装最新版本的 [node.js](https://nodejs.org/)
    * 安装 [GIT](https://git-scm.com/download)
    * 如果使用 Linux 系统，还需要安装 Wine
* 推荐配置
    * 安装 [VSCode](https://code.visualstudio.com/)

克隆此仓库以下载：
```bash
git clone https://gitee.com/projectxero/bdsx-cn-mirror.git
```

### 使用 VSCode 调试与运行
如果需要使用 VSCode 启动 BDSX，您需要
1. 使用 VSCode 打开此项目
2. 安装 Node Debug (legacy)。您可以在右下方看到建议通知框。
3. 打开终端（Ctrl+Shift+｀）
4. 运行 `npm i` 来安装 NPM 包与 BDS
5. 按下 `F5` 以构建并运行

### 直接运行
运行 `bdsx.bat`（或 `bdsx.sh`，如果使用 Linux 系统）以启动 BDSX

### 手动运行
1. 在 bdsx 目录下打开终端
2. 运行 `npm i` 来安装 NPM 包与 BDS
3. 运行 `tsc` 以编译 TypeScript，随后在 bedrock_server 目录中运行 `bedrock_server.exe ..`。如果使用 Linux 系统，则使用 `wine bedrock_server.exe ..`

## 目录结构
```sh
[bdsx项目]
├ [bdsx] # 核心库
├ [example_and_test] # 一些 BDSX API 的使用例与测试
├ [bedrock_server] # BDS
├ launcher.ts # BDS 脚本启动器
├ index.ts # 主入口点。此文件会在 BDS 完全启动后被 BDS 脚本启动器调用
├ bdsx.sh # Linux 系统的可执行文件
└ bdsx.bat # Windows 系统的可执行文件
```
> 请在 ./index.ts 中编写您的代码

> 默认 index.ts 会调用 example_and_test。您可以通过移除调用的那一行或者将其替换为自己的代码以禁用例子与测试。

> 如果需要例子，请查看 `example_and_test` 目录。此外，NPM 上的 [@bdsx](https://www.npmjs.com/search?q=@bdsx) 组织内也有部分插件可供使用。

## 创建 BDSX 插件
请参阅 [`plugin-example/README.md`](plugin-example/README.md).

## QQ 群
[BDSX开发讨论群：628811617](https://jq.qq.com/?_wv=1027&k=02keC27c)

## BDSX Discussions
https://github.com/bdsx/bdsx/discussions

## BDSX Wiki
https://github.com/bdsx/bdsx/wiki

## Docker 镜像
https://hub.docker.com/r/karikera/bdsx

## Bug 反馈
https://github.com/bdsx/bdsx/issues

## BDSX Core
https://github.com/bdsx/bdsx-core
